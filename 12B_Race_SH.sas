﻿/*---------------------------------------------------------------------------------
Project:            0979-grifols-albumin-lvp

Program:            ____ 

Function:           ____

Dependencies:		____

Dependents:			____

Author:             ____

Date Completed:     ____

SAS Version:        ____   

Platform:           ____

Macros Called:      ____ 

Macro Parameters:   ____

Comments:           ____

Example:			____

Revisions:
date	name	what
210402	SH		Initial verson
---------------------------------------------------------------------------------*/


* ==================================  Step 1. Import  ================================== *;
proc import datafile="C:\Users\&SYSUSERID.\Documents\0979-grifols-albumin-lvp\Export\Race1.csv"
	out=Race2 dbms=csv replace;
    getnames=yes;
run;
* R = 6677 *;

proc contents data=Race2 position;
run;
* race => only 254 characters *;

data Race3;
	length
		personid $ 36
		race $ 10000
	;
	infile
		"C:\Users\&SYSUSERID.\Documents\0979-grifols-albumin-lvp\Export\Race1.csv"
		TRUNCOVER
		FIRSTOBS=2
	;
	input
		@1 personid $36.
		@38 race $10000.
	;
run;
* R = 6677 *;

proc sql;
	create table Race4 as
	select 	distinct personid,
			lower(race) as race
	from Race3
	order by 1, 2;
quit;
* R = 6677 *;



* ==================================  Step 2. Reclassification  ================================== *;
/*
White
Caucasian

African American
Black
Black or African American

American Indian
American Indian or Alaska Native
Asian
Burmese
European
Hispanic
Hispanic, black
Indian
Korean
Mixed racial group
Native Hawaiian
Native Hawaiian or Other Pacific Islander
Other Race

null
Patient data refused
Patient not asked
Race not stated
Unknown racial group
*/

* ========================  Step 2-1. White  ======================== *;
proc sql;
	create table zzz1 as
	select 	distinct race
	from Race4
	where 	race like "%%white%" or
			race like "%%caucasian%"
	order by race;
quit;
* R = 191 *;

proc sql;
	create table white as
	select 	distinct personid
	from Race4
	where 	race like "%%white%" or
			race like "%%caucasian%"
	order by personid;
quit;
* R = 5355 *;



* ========================  Step 2-2. Black  ======================== *;
proc sql;
	create table zzz2 as
	select 	distinct race
	from Race4
	/*where race like "%%african american%"*/
	where 	race like "%%black%" or
			race like "%%african american%"
	order by race;
quit;
* R = 56 *;

proc sql;
	create table black as
	select 	distinct personid
	from Race4
	where 	race like "%%black%" or
			race like "%%african american%"
	order by personid;
quit;
* R = 409 *;



* ========================  Step 2-3. Other  ======================== *;
proc sql;
	create table zzz3 as
	select 	distinct race
	from Race4
	where 	race like "%%american indian%" or
			race like "%%asian%" or
			race like "%%burmese%" or
			race like "%%european%" or
			race like "%%hispanic%" or
			race like "%%indian%" or
			race like "%%korean%" or
			race like "%%mixed racial group%" or
			race like "%%native hawaiian%" or
			race like "%%other race%"
	order by race;
quit;
* R = 135 *;

proc sql;
	create table other as
	select 	distinct personid
	from Race4
	where 	race like "%%american indian%" or
			race like "%%asian%" or
			race like "%%burmese%" or
			race like "%%european%" or
			race like "%%hispanic%" or
			race like "%%indian%" or
			race like "%%korean%" or
			race like "%%mixed racial group%" or
			race like "%%native hawaiian%" or
			race like "%%other race%"
	order by personid;
quit;
* R = 994 *;


* ========================  Step 2-4. Combine  ======================== *;
proc sql;
	create table Race5 as
	select 	t1.personid,
			(t2.personid ne "") as white format=1.,
			(t3.personid ne "") as black format=1.,
			(t4.personid ne "") as other format=1.
	from Race4 as t1 left join white as t2 on t1.personid = t2.personid
					 left join black as t3 on t1.personid = t3.personid
					 left join other as t4 on t1.personid = t4.personid
	order by t1.personid;
quit;
* R = 6677 *;

proc sql;
	select 	distinct white,
			black,
			other
	from Race5
	order by 1 desc, 2 desc, 3 desc;
quit;
/*
white black other 
1 1 1 
1 1 0 
1 0 1 
1 0 0 
0 1 1 
0 1 0 
0 0 1 
0 0 0 
*/

data Race6;
	set Race5;
	if white = 1 and black = 1 and other = 1 then race_group = 3;
	if white = 1 and black = 1 and other = 0 then race_group = 3;
	if white = 1 and black = 0 and other = 1 then race_group = 3;
	if white = 1 and black = 0 and other = 0 then race_group = 1;
	if white = 0 and black = 1 and other = 1 then race_group = 3;
	if white = 0 and black = 1 and other = 0 then race_group = 2;
	if white = 0 and black = 0 and other = 1 then race_group = 3;
	if white = 0 and black = 0 and other = 0 then race_group = 4;

	/*if white = 1 and black = 1 and other = 1 then race_group = 3;
	if white = 1 and black = 1 and other = 0 then race_group = 3;
	if white = 1 and black = 0 and other = 1 then race_group = 1;
	if white = 1 and black = 0 and other = 0 then race_group = 1;
	if white = 0 and black = 1 and other = 1 then race_group = 2;
	if white = 0 and black = 1 and other = 0 then race_group = 2;
	if white = 0 and black = 0 and other = 1 then race_group = 3;
	if white = 0 and black = 0 and other = 0 then race_group = 4;*/
run;

proc freq data=Race6;
	table race_group / missing norow nocol;
run;
/*
1 5217 78.13 5217 78.13 
2 387 5.80 5604 83.93 
3 1007 15.08 6611 99.01 
4 66 0.99 6677 100.00
*/

proc export data=Race6
	dbms=csv
	outfile="C:\Users\&SYSUSERID.\Documents\0979-grifols-albumin-lvp\Import\Race_Group.csv"
	replace;
run;

*********************************************  End  *********************************************;
