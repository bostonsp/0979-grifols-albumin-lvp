﻿/*---------------------------------------------------------------------------------
Project:            0979-grifols-albumin-lvp

Program:            ____ 

Function:           ____

Dependencies:		____

Dependents:			____

Author:             ____

Date Completed:     ____

SAS Version:        ____   

Platform:           ____

Macros Called:      ____ 

Macro Parameters:   ____

Comments:           ____

Example:			____

Revisions:
date	name	what
210420	SH		Initial verson
---------------------------------------------------------------------------------*/


* ==================================  Step 1. Import  ================================== *;
proc import datafile="C:\Users\&SYSUSERID.\Documents\0979-grifols-albumin-lvp\Export\FU2.csv"
	out=FU2 dbms=csv replace;
    getnames=yes;
run;
* R = 6698, C = 37 *;

proc contents data=FU2 position;
run;
/*
1 personid Char 36 $36. $36. 
2 encounterid Char 36 $36. $36. 
3 LVP_ALB Num 8 BEST12. BEST32. 
4 caresetting Char 10 $10. $10. 
5 FU3 Num 8 BEST12. BEST32. 
6 FU6 Num 8 BEST12. BEST32. 
7 FU12 Num 8 BEST12. BEST32. 
8 LVP_post3 Num 8 BEST12. BEST32. 
9 LVP_post6 Num 8 BEST12. BEST32. 
10 LVP_post12 Num 8 BEST12. BEST32. 
11 HCRU_OP_post3 Num 8 BEST12. BEST32. 
12 HCRU_ER_post3 Num 8 BEST12. BEST32. 
13 HCRU_IP_post3 Num 8 BEST12. BEST32. 
14 HCRU_OP_post6 Num 8 BEST12. BEST32. 
15 HCRU_ER_post6 Num 8 BEST12. BEST32. 
16 HCRU_IP_post6 Num 8 BEST12. BEST32. 
17 HCRU_OP_post12 Num 8 BEST12. BEST32. 
18 HCRU_ER_post12 Num 8 BEST12. BEST32. 
19 HCRU_IP_post12 Num 8 BEST12. BEST32. 
20 Ascites_post3 Num 8 BEST12. BEST32. 
21 Ascites_post6 Num 8 BEST12. BEST32. 
22 Ascites_post12 Num 8 BEST12. BEST32. 
23 SBP_post3 Num 8 BEST12. BEST32. 
24 SBP_post6 Num 8 BEST12. BEST32. 
25 SBP_post12 Num 8 BEST12. BEST32. 
26 HRS_post3 Num 8 BEST12. BEST32. 
27 HRS_post6 Num 8 BEST12. BEST32. 
28 HRS_post12 Num 8 BEST12. BEST32. 
29 AKI_w_ESRD_post3 Num 8 BEST12. BEST32. 
30 AKI_w_ESRD_post6 Num 8 BEST12. BEST32. 
31 AKI_w_ESRD_post12 Num 8 BEST12. BEST32. 
32 AKI_wo_ESRD_post3 Num 8 BEST12. BEST32. 
33 AKI_wo_ESRD_post6 Num 8 BEST12. BEST32. 
34 AKI_wo_ESRD_post12 Num 8 BEST12. BEST32. 
35 Hyponatermia_post3 Num 8 BEST12. BEST32. 
36 Hyponatermia_post6 Num 8 BEST12. BEST32. 
37 Hyponatermia_post12 Num 8 BEST12. BEST32. 
*/

proc freq data=FU2;
	table caresetting / missing norow nocol;
run;

data FU3;
	set FU2;
	if caresetting = "Outpatient" then caresetting_cat = 1;
	else if caresetting = "Emergency" then caresetting_cat = 2;
	else if caresetting = "Inpatient" then caresetting_cat = 3;
	else if caresetting = "Admitted f" then caresetting_cat = 4;
run;

proc freq data=FU3;
	table caresetting_cat / missing norow nocol;
run;



* ==================================  Step 2. Summary Statistics  ================================== *;
* ========================  Step 2-1. LVP  ======================== *;
/*
LVP_post3 
LVP_post6 
LVP_post12
*/

proc means data=FU3 n mean std median q1 q3;
	var LVP_post3;
	where LVP_post3 > 0;
run; 

proc means data=FU3 n mean std median q1 q3;
	class LVP_ALB;
	var LVP_post3;
	where LVP_post3 > 0;
run; 

proc means data=FU3 n mean std median q1 q3;
	class caresetting_cat;
	var LVP_post3;
	where LVP_post3 > 0;
run; 

proc means data=FU3 n mean std median q1 q3;
	class caresetting_cat LVP_ALB;
	var LVP_post3;
	where LVP_post3 > 0;
run;


* ========================  Step 2-2. HCRU  ======================== *;
/*
HCRU_OP_post3 
HCRU_ER_post3 
HCRU_IP_post3

HCRU_OP_post6
HCRU_ER_post6 
HCRU_IP_post6

HCRU_OP_post12 
HCRU_ER_post12 
HCRU_IP_post12
*/

proc means data=FU3 n mean std median q1 q3;
	var HCRU_OP_post3;
	where HCRU_OP_post3 > 0;
run; 

proc means data=FU3 n mean std median q1 q3;
	class LVP_ALB;
	var HCRU_OP_post3;
	where HCRU_OP_post3 > 0;
run; 

proc means data=FU3 n mean std median q1 q3;
	class caresetting_cat;
	var HCRU_OP_post3;
	where HCRU_OP_post3 > 0;
run; 

proc means data=FU3 n mean std median q1 q3;
	class caresetting_cat LVP_ALB;
	var HCRU_OP_post3;
	where HCRU_OP_post3 > 0;
run; 


* ========================  Step 2-3. Comorbidities and Complications  ======================== *;
* ==============  Step 2-3-1. FU3  ======================== *;
proc freq data=FU3;
	table Ascites_post3 SBP_post3 HRS_post3 AKI_w_ESRD_post3 AKI_wo_ESRD_post3 Hyponatermia_post3 / missing norow nocol;
	where FU3 = 1 and LVP_ALB = 0;
run;
proc freq data=FU3;
	table Ascites_post3 SBP_post3 HRS_post3 AKI_w_ESRD_post3 AKI_wo_ESRD_post3 Hyponatermia_post3 / missing norow nocol;
	where FU3 = 1 and LVP_ALB = 1;
run;
proc freq data=FU3;
	table Ascites_post3 SBP_post3 HRS_post3 AKI_w_ESRD_post3 AKI_wo_ESRD_post3 Hyponatermia_post3 / missing norow nocol;
	where FU3 = 1;
run;

proc freq data=FU3;
	table Ascites_post3 SBP_post3 HRS_post3 AKI_w_ESRD_post3 AKI_wo_ESRD_post3 Hyponatermia_post3 / missing norow nocol;
	where FU3 = 1 and LVP_ALB = 0 and caresetting_cat = 1;
run;
proc freq data=FU3;
	table Ascites_post3 SBP_post3 HRS_post3 AKI_w_ESRD_post3 AKI_wo_ESRD_post3 Hyponatermia_post3 / missing norow nocol;
	where FU3 = 1 and LVP_ALB = 1 and caresetting_cat = 1;
run;
proc freq data=FU3;
	table Ascites_post3 SBP_post3 HRS_post3 AKI_w_ESRD_post3 AKI_wo_ESRD_post3 Hyponatermia_post3 / missing norow nocol;
	where FU3 = 1 and caresetting_cat = 1;
run;

proc freq data=FU3;
	table Ascites_post3 SBP_post3 HRS_post3 AKI_w_ESRD_post3 AKI_wo_ESRD_post3 Hyponatermia_post3 / missing norow nocol;
	where FU3 = 1 and LVP_ALB = 0 and caresetting_cat = 2;
run;
proc freq data=FU3;
	table Ascites_post3 SBP_post3 HRS_post3 AKI_w_ESRD_post3 AKI_wo_ESRD_post3 Hyponatermia_post3 / missing norow nocol;
	where FU3 = 1 and LVP_ALB = 1 and caresetting_cat = 2;
run;
proc freq data=FU3;
	table Ascites_post3 SBP_post3 HRS_post3 AKI_w_ESRD_post3 AKI_wo_ESRD_post3 Hyponatermia_post3 / missing norow nocol;
	where FU3 = 1 and caresetting_cat = 2;
run;

proc freq data=FU3;
	table Ascites_post3 SBP_post3 HRS_post3 AKI_w_ESRD_post3 AKI_wo_ESRD_post3 Hyponatermia_post3 / missing norow nocol;
	where FU3 = 1 and LVP_ALB = 0 and caresetting_cat = 3;
run;
proc freq data=FU3;
	table Ascites_post3 SBP_post3 HRS_post3 AKI_w_ESRD_post3 AKI_wo_ESRD_post3 Hyponatermia_post3 / missing norow nocol;
	where FU3 = 1 and LVP_ALB = 1 and caresetting_cat = 3;
run;
proc freq data=FU3;
	table Ascites_post3 SBP_post3 HRS_post3 AKI_w_ESRD_post3 AKI_wo_ESRD_post3 Hyponatermia_post3 / missing norow nocol;
	where FU3 = 1 and caresetting_cat = 3;
run;

proc freq data=FU3;
	table Ascites_post3 SBP_post3 HRS_post3 AKI_w_ESRD_post3 AKI_wo_ESRD_post3 Hyponatermia_post3 / missing norow nocol;
	where FU3 = 1 and LVP_ALB = 0 and caresetting_cat = 4;
run;
proc freq data=FU3;
	table Ascites_post3 SBP_post3 HRS_post3 AKI_w_ESRD_post3 AKI_wo_ESRD_post3 Hyponatermia_post3 / missing norow nocol;
	where FU3 = 1 and LVP_ALB = 1 and caresetting_cat = 4;
run;
proc freq data=FU3;
	table Ascites_post3 SBP_post3 HRS_post3 AKI_w_ESRD_post3 AKI_wo_ESRD_post3 Hyponatermia_post3 / missing norow nocol;
	where FU3 = 1 and caresetting_cat = 4;
run;


* ==============  Step 2-3-2. FU6  ======================== *;
proc freq data=FU3;
	table Ascites_post6 SBP_post6 HRS_post6 AKI_w_ESRD_post6 AKI_wo_ESRD_post6 Hyponatermia_post6 / missing norow nocol;
	where FU6 = 1 and LVP_ALB = 0;
run;
proc freq data=FU3;
	table Ascites_post6 SBP_post6 HRS_post6 AKI_w_ESRD_post6 AKI_wo_ESRD_post6 Hyponatermia_post6 / missing norow nocol;
	where FU6 = 1 and LVP_ALB = 1;
run;
proc freq data=FU3;
	table Ascites_post6 SBP_post6 HRS_post6 AKI_w_ESRD_post6 AKI_wo_ESRD_post6 Hyponatermia_post6 / missing norow nocol;
	where FU6 = 1;
run;

proc freq data=FU3;
	table Ascites_post6 SBP_post6 HRS_post6 AKI_w_ESRD_post6 AKI_wo_ESRD_post6 Hyponatermia_post6 / missing norow nocol;
	where FU6 = 1 and LVP_ALB = 0 and caresetting_cat = 1;
run;
proc freq data=FU3;
	table Ascites_post6 SBP_post6 HRS_post6 AKI_w_ESRD_post6 AKI_wo_ESRD_post6 Hyponatermia_post6 / missing norow nocol;
	where FU6 = 1 and LVP_ALB = 1 and caresetting_cat = 1;
run;
proc freq data=FU3;
	table Ascites_post6 SBP_post6 HRS_post6 AKI_w_ESRD_post6 AKI_wo_ESRD_post6 Hyponatermia_post6 / missing norow nocol;
	where FU6 = 1 and caresetting_cat = 1;
run;

proc freq data=FU3;
	table Ascites_post6 SBP_post6 HRS_post6 AKI_w_ESRD_post6 AKI_wo_ESRD_post6 Hyponatermia_post6 / missing norow nocol;
	where FU6 = 1 and LVP_ALB = 0 and caresetting_cat = 2;
run;
proc freq data=FU3;
	table Ascites_post6 SBP_post6 HRS_post6 AKI_w_ESRD_post6 AKI_wo_ESRD_post6 Hyponatermia_post6 / missing norow nocol;
	where FU6 = 1 and LVP_ALB = 1 and caresetting_cat = 2;
run;
proc freq data=FU3;
	table Ascites_post6 SBP_post6 HRS_post6 AKI_w_ESRD_post6 AKI_wo_ESRD_post6 Hyponatermia_post6 / missing norow nocol;
	where FU6 = 1 and caresetting_cat = 2;
run;

proc freq data=FU3;
	table Ascites_post6 SBP_post6 HRS_post6 AKI_w_ESRD_post6 AKI_wo_ESRD_post6 Hyponatermia_post6 / missing norow nocol;
	where FU6 = 1 and LVP_ALB = 0 and caresetting_cat = 3;
run;
proc freq data=FU3;
	table Ascites_post6 SBP_post6 HRS_post6 AKI_w_ESRD_post6 AKI_wo_ESRD_post6 Hyponatermia_post6 / missing norow nocol;
	where FU6 = 1 and LVP_ALB = 1 and caresetting_cat = 3;
run;
proc freq data=FU3;
	table Ascites_post6 SBP_post6 HRS_post6 AKI_w_ESRD_post6 AKI_wo_ESRD_post6 Hyponatermia_post6 / missing norow nocol;
	where FU6 = 1 and caresetting_cat = 3;
run;

proc freq data=FU3;
	table Ascites_post6 SBP_post6 HRS_post6 AKI_w_ESRD_post6 AKI_wo_ESRD_post6 Hyponatermia_post6 / missing norow nocol;
	where FU6 = 1 and LVP_ALB = 0 and caresetting_cat = 4;
run;
proc freq data=FU3;
	table Ascites_post6 SBP_post6 HRS_post6 AKI_w_ESRD_post6 AKI_wo_ESRD_post6 Hyponatermia_post6 / missing norow nocol;
	where FU6 = 1 and LVP_ALB = 1 and caresetting_cat = 4;
run;
proc freq data=FU3;
	table Ascites_post6 SBP_post6 HRS_post6 AKI_w_ESRD_post6 AKI_wo_ESRD_post6 Hyponatermia_post6 / missing norow nocol;
	where FU6 = 1 and caresetting_cat = 4;
run;


* ==============  Step 2-3-3. FU12  ======================== *;
proc freq data=FU3;
	table Ascites_post12 SBP_post12 HRS_post12 AKI_w_ESRD_post12 AKI_wo_ESRD_post12 Hyponatermia_post12 / missing norow nocol;
	where FU12 = 1 and LVP_ALB = 0;
run;
proc freq data=FU3;
	table Ascites_post12 SBP_post12 HRS_post12 AKI_w_ESRD_post12 AKI_wo_ESRD_post12 Hyponatermia_post12 / missing norow nocol;
	where FU12 = 1 and LVP_ALB = 1;
run;
proc freq data=FU3;
	table Ascites_post12 SBP_post12 HRS_post12 AKI_w_ESRD_post12 AKI_wo_ESRD_post12 Hyponatermia_post12 / missing norow nocol;
	where FU12 = 1;
run;

proc freq data=FU3;
	table Ascites_post12 SBP_post12 HRS_post12 AKI_w_ESRD_post12 AKI_wo_ESRD_post12 Hyponatermia_post12 / missing norow nocol;
	where FU12 = 1 and LVP_ALB = 0 and caresetting_cat = 1;
run;
proc freq data=FU3;
	table Ascites_post12 SBP_post12 HRS_post12 AKI_w_ESRD_post12 AKI_wo_ESRD_post12 Hyponatermia_post12 / missing norow nocol;
	where FU12 = 1 and LVP_ALB = 1 and caresetting_cat = 1;
run;
proc freq data=FU3;
	table Ascites_post12 SBP_post12 HRS_post12 AKI_w_ESRD_post12 AKI_wo_ESRD_post12 Hyponatermia_post12 / missing norow nocol;
	where FU12 = 1 and caresetting_cat = 1;
run;

proc freq data=FU3;
	table Ascites_post12 SBP_post12 HRS_post12 AKI_w_ESRD_post12 AKI_wo_ESRD_post12 Hyponatermia_post12 / missing norow nocol;
	where FU12 = 1 and LVP_ALB = 0 and caresetting_cat = 2;
run;
proc freq data=FU3;
	table Ascites_post12 SBP_post12 HRS_post12 AKI_w_ESRD_post12 AKI_wo_ESRD_post12 Hyponatermia_post12 / missing norow nocol;
	where FU12 = 1 and LVP_ALB = 1 and caresetting_cat = 2;
run;
proc freq data=FU3;
	table Ascites_post12 SBP_post12 HRS_post12 AKI_w_ESRD_post12 AKI_wo_ESRD_post12 Hyponatermia_post12 / missing norow nocol;
	where FU12 = 1 and caresetting_cat = 2;
run;

proc freq data=FU3;
	table Ascites_post12 SBP_post12 HRS_post12 AKI_w_ESRD_post12 AKI_wo_ESRD_post12 Hyponatermia_post12 / missing norow nocol;
	where FU12 = 1 and LVP_ALB = 0 and caresetting_cat = 3;
run;
proc freq data=FU3;
	table Ascites_post12 SBP_post12 HRS_post12 AKI_w_ESRD_post12 AKI_wo_ESRD_post12 Hyponatermia_post12 / missing norow nocol;
	where FU12 = 1 and LVP_ALB = 1 and caresetting_cat = 3;
run;
proc freq data=FU3;
	table Ascites_post12 SBP_post12 HRS_post12 AKI_w_ESRD_post12 AKI_wo_ESRD_post12 Hyponatermia_post12 / missing norow nocol;
	where FU12 = 1 and caresetting_cat = 3;
run;

proc freq data=FU3;
	table Ascites_post12 SBP_post12 HRS_post12 AKI_w_ESRD_post12 AKI_wo_ESRD_post12 Hyponatermia_post12 / missing norow nocol;
	where FU12 = 1 and LVP_ALB = 0 and caresetting_cat = 4;
run;
proc freq data=FU3;
	table Ascites_post12 SBP_post12 HRS_post12 AKI_w_ESRD_post12 AKI_wo_ESRD_post12 Hyponatermia_post12 / missing norow nocol;
	where FU12 = 1 and LVP_ALB = 1 and caresetting_cat = 4;
run;
proc freq data=FU3;
	table Ascites_post12 SBP_post12 HRS_post12 AKI_w_ESRD_post12 AKI_wo_ESRD_post12 Hyponatermia_post12 / missing norow nocol;
	where FU12 = 1 and caresetting_cat = 4;
run;

*********************************************  End  *********************************************;
