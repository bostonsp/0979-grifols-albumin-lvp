﻿/*---------------------------------------------------------------------------------
Project:            0979-grifols-albumin-lvp

Program:            ____ 

Function:           ____

Dependencies:		____

Dependents:			____

Author:             ____

Date Completed:     ____

SAS Version:        ____   

Platform:           ____

Macros Called:      ____ 

Macro Parameters:   ____

Comments:           Reference: https://support.sas.com/resources/papers/proceedings/proceedings/sugi31/048-31.pdf

Example:			____

Revisions:
date	name	what
210420	SH		Initial verson
---------------------------------------------------------------------------------*/

libname dtout "F:\0979\Data";



* ==================================  Step 1. Import  ================================== *;
proc import datafile="C:\Users\&SYSUSERID.\Documents\0979-grifols-albumin-lvp\Export\FU2.csv"
	out=FU2 dbms=csv replace;
    getnames=yes;
run;
* R = 6698, C = 37 *;

proc import datafile="C:\Users\&SYSUSERID.\Documents\0979-grifols-albumin-lvp\Export\LOS1.csv"
	out=LOS1 dbms=csv replace;
    getnames=yes;
run;
* R = 34275, C = 11 *;

proc import datafile="C:\Users\&SYSUSERID.\Documents\0979-grifols-albumin-lvp\Export\LOS2.csv"
	out=LOS2 dbms=csv replace;
    getnames=yes;
run;
* R = 51250, C = 11 *;

proc import datafile="C:\Users\&SYSUSERID.\Documents\0979-grifols-albumin-lvp\Export\LOS3.csv"
	out=LOS3 dbms=csv replace;
    getnames=yes;
run;
* R = 71445, C = 11 *;

proc contents data=LOS1 position;
run;
/*
1 personid Char 36 $36. $36. 
2 encounterid Char 36 $36. $36. 
3 start_datetime Num 8 DATETIME. ANYDTDTM40. 
4 end_datetime Num 8 DATETIME. ANYDTDTM40. 
5 LVP_ALB Num 8 BEST12. BEST32. 
6 caresetting Char 10 $10. $10. 
7 FU_encounterid Char 36 $36. $36. 
8 FU_start_datetime Num 8 DATETIME. ANYDTDTM40. 
9 FU_end_datetime Num 8 DATETIME. ANYDTDTM40. 
10 FU_LOS_day Num 8 BEST12. BEST32. 
11 FU_caresetting Char 17 $17. $17.
*/

proc freq data=LOS1;
	table caresetting / missing norow nocol;
run;
/*
Admitted f 554 1.62 554 1.62 
Emergency 5634 16.44 6188 18.05 
Inpatient 12616 36.81 18804 54.86 
Outpatient 15471 45.14 34275 100.00 
*/

proc freq data=LOS2;
	table caresetting / missing norow nocol;
run;
/*
Admitted f 862 1.68 862 1.68 
Emergency 8332 16.26 9194 17.94 
Inpatient 18608 36.31 27802 54.25 
Outpatient 23448 45.75 51250 100.00 
*/

proc freq data=LOS3;
	table caresetting / missing norow nocol;
run;
/*
Admitted 1255 1.76 1255 1.76 
Emergency 11678 16.35 12933 18.10 
Inpatient 25441 35.61 38374 53.71 
Outpatien 33071 46.29 71445 100.00 
*/

proc freq data=LOS3;
	table FU_caresetting * caresetting / missing norow nocol nocum nopercent;
run;



* ==================================  Step 2. Outliers  ================================== *;
data zzz1;
	set LOS1;
	FU_LOS_HOUR = FU_LOS_day * 24;
run;
proc means data=zzz1 n nmiss mean std min p1 p5 p10 p25 p50 p75 p90 p95 p99 max;
	class FU_caresetting;
	var FU_LOS_HOUR;
run;

data zzz2;
	set LOS2;
	FU_LOS_HOUR = FU_LOS_day * 24;
run;
proc means data=zzz2 n nmiss mean std min p1 p5 p10 p25 p50 p75 p90 p95 p99 max;
	class FU_caresetting;
	var FU_LOS_HOUR;
run;

data zzz3;
	set LOS3;
	FU_LOS_HOUR = FU_LOS_day * 24;
run;
proc means data=zzz2 n nmiss mean std min p1 p5 p10 p25 p50 p75 p90 p95 p99 max;
	class FU_caresetting;
	var FU_LOS_HOUR;
run;



* ==================================  Step 3. LOS Macro  ================================== *;
%macro LOS_Macro(data=, caresetting=, output=);

proc sql;
	create table temp1 as
	select	personid,
			encounterid,
			start_datetime,
			end_datetime,
			LVP_ALB,
			caresetting,
			FU_encounterid,
			FU_start_datetime,
			FU_end_datetime,
			FU_LOS_day,
			FU_caresetting
	from &data
	where FU_caresetting in ("Inpatient")
	/*where FU_caresetting in ("Inpatient", "Admitted for Obse")*/
	order by personid, encounterid, FU_start_datetime, FU_end_datetime;
quit;

proc sql;
	create table temp2 as
	select	distinct personid,
			end_datetime,
			LVP_ALB,
			FU_start_datetime,
			FU_end_datetime
	from temp1
	&caresetting
	order by personid, FU_start_datetime, FU_end_datetime;
quit;

data temp3;
	set temp2;
	*if (FU_end_datetime - end_datetime) / (60*60*24) > 90;

	format start_time end_time datetime20.;
	start_time = FU_start_datetime;
	if (FU_end_datetime - end_datetime) / (60*60*24) > 90 then end_time = end_datetime + (60*60*24*90);
	else end_time = FU_end_datetime;
run;

proc sql;
	create table temp4 as
	select	distinct personid,
			LVP_ALB,

			start_time,
			end_time,

			round(start_time, 60) / 60 as start_minute,
			round(end_time, 60) / 60 as end_minute
	from temp3
	order by 1, 2, 5, 6;
quit;

proc transpose data=temp4 out=temp5A (drop=_name_) prefix=start;
	 by personid LVP_ALB;
	 var start_minute;
run;
proc transpose data=temp4 out=temp5B (drop=_name_) prefix=stop;
	 by personid LVP_ALB;
	 var end_minute;
run;

data temp6;
	 merge temp5A temp5B;
	 by personid LVP_ALB;
run;

data &output;
	retain i duration;
	set temp6; *--- Use Horizontal Data Representation. ---*;
	duration = 0;

	array start{*} start:;
	array stop{*} stop:;

	*--- Find the Maximum Possible AE Interval. ---*;
	intv_num = dim(start);
	minstart = min(of start:);
	maxstop = max(of stop:);

	*--- Check one day at a time from the Maximum Possible AE Interval. ---*;
	do xdate = minstart to maxstop;
		i = 1;
		in_interval = 0;

		*--- Check if a date is within any interval. ---*;
		*--- Stop checking once a date has been found within an interval or ---*;
		*--- all dates have been checked and the date has not yet been found. ---*;
		do while (in_interval = 0 and i <= intv_num);
			if (start(i) <= xdate <= stop(i)) then in_interval = 1;
			i + 1;
		end;

		if in_interval = 1 then duration + 1;
	end;

	LOS = duration / (60*24);
run;

%mend LOS_Macro;

%LOS_Macro(data=LOS1, caresetting=where caresetting ne "", output=dtout.LOS3_ALL);
%LOS_Macro(data=LOS1, caresetting=where caresetting = "Outpatient", output=dtout.LOS3_OP);
%LOS_Macro(data=LOS1, caresetting=where caresetting = "Emergency", output=dtout.LOS3_ER);
%LOS_Macro(data=LOS1, caresetting=where caresetting = "Inpatient", output=dtout.LOS3_IP);
%LOS_Macro(data=LOS1, caresetting=where caresetting = "Admitted f", output=dtout.LOS3_OB);

%LOS_Macro(data=LOS2, caresetting=where caresetting ne "", output=dtout.LOS6_ALL);
%LOS_Macro(data=LOS2, caresetting=where caresetting = "Outpatient", output=dtout.LOS6_OP);
%LOS_Macro(data=LOS2, caresetting=where caresetting = "Emergency", output=dtout.LOS6_ER);
%LOS_Macro(data=LOS2, caresetting=where caresetting = "Inpatient", output=dtout.LOS6_IP);
%LOS_Macro(data=LOS2, caresetting=where caresetting = "Admitted f", output=dtout.LOS6_OB);

%LOS_Macro(data=LOS3, caresetting=where caresetting ne "", output=dtout.LOS12_ALL);
%LOS_Macro(data=LOS3, caresetting=where caresetting = "Outpatien", output=dtout.LOS12_OP);
%LOS_Macro(data=LOS3, caresetting=where caresetting = "Emergency", output=dtout.LOS12_ER);
%LOS_Macro(data=LOS3, caresetting=where caresetting = "Inpatient", output=dtout.LOS12_IP);
%LOS_Macro(data=LOS3, caresetting=where caresetting = "Admitted", output=dtout.LOS12_OB);



* ==================================  Step 4. LOS Table  ================================== *;
proc sql;
	create table zzz1A as
	select	distinct t1.personid,
			t1.LVP_ALB,
			t1.caresetting,

			t21.LOS as LOS3_ALL,
			t22.LOS as LOS3_OP,
			t23.LOS as LOS3_ER,
			t24.LOS as LOS3_IP,
			t25.LOS as LOS3_OB
	from FU2 as t1 
		left join dtout.LOS3_ALL as t21 on t1.personid = t21.personid
		left join dtout.LOS3_OP as t22 on t1.personid = t22.personid
		left join dtout.LOS3_ER as t23 on t1.personid = t23.personid
		left join dtout.LOS3_IP as t24 on t1.personid = t24.personid
		left join dtout.LOS3_OB as t25 on t1.personid = t25.personid
	where t1.FU3 = 1
	order by t1.personid;
quit;
data zzz1B;
	set zzz1A;
	if LOS3_ALL = . then LOS3_ALL = 0;
	if LOS3_OP = . and caresetting = "Outpatient" then LOS3_OP = 0;
	if LOS3_ER = . and caresetting = "Emergency" then LOS3_ER = 0;
	if LOS3_IP = . and caresetting = "Inpatient" then LOS3_IP = 0;
	if LOS3_OB = . and caresetting = "Admitted f" then LOS3_OB = 0;
run;
* R = 4415 *;

proc means data=zzz1B n nmiss mean std median q1 q3 maxdec=1;
	class LVP_ALB;
	var LOS:;
run;
proc means data=zzz1B n nmiss mean std median q1 q3 maxdec=1;
	var LOS:;
run;

proc sql;
	create table zzz2A as
	select	distinct t1.personid,
			t1.LVP_ALB,
			t1.caresetting,

			t21.LOS as LOS6_ALL,
			t22.LOS as LOS6_OP,
			t23.LOS as LOS6_ER,
			t24.LOS as LOS6_IP,
			t25.LOS as LOS6_OB
	from FU2 as t1 
		left join dtout.LOS6_ALL as t21 on t1.personid = t21.personid
		left join dtout.LOS6_OP as t22 on t1.personid = t22.personid
		left join dtout.LOS6_ER as t23 on t1.personid = t23.personid
		left join dtout.LOS6_IP as t24 on t1.personid = t24.personid
		left join dtout.LOS6_OB as t25 on t1.personid = t25.personid
	where t1.FU6 = 1
	order by t1.personid;
quit;
data zzz2B;
	set zzz2A;
	if LOS6_ALL = . then LOS6_ALL = 0;
	if LOS6_OP = . and caresetting = "Outpatient" then LOS6_OP = 0;
	if LOS6_ER = . and caresetting = "Emergency" then LOS6_ER = 0;
	if LOS6_IP = . and caresetting = "Inpatient" then LOS6_IP = 0;
	if LOS6_OB = . and caresetting = "Admitted f" then LOS6_OB = 0;
run;
* R = 3703 *;

proc means data=zzz2B n nmiss mean std median q1 q3 maxdec=1;
	class LVP_ALB;
	var LOS:;
run;
proc means data=zzz2B n nmiss mean std median q1 q3 maxdec=1;
	var LOS:;
run;

proc sql;
	create table zzz3A as
	select	distinct t1.personid,
			t1.LVP_ALB,
			t1.caresetting,

			t21.LOS as LOS12_ALL,
			t22.LOS as LOS12_OP,
			t23.LOS as LOS12_ER,
			t24.LOS as LOS12_IP,
			t25.LOS as LOS12_OB
	from FU2 as t1 
		left join dtout.LOS12_ALL as t21 on t1.personid = t21.personid
		left join dtout.LOS12_OP as t22 on t1.personid = t22.personid
		left join dtout.LOS12_ER as t23 on t1.personid = t23.personid
		left join dtout.LOS12_IP as t24 on t1.personid = t24.personid
		left join dtout.LOS12_OB as t25 on t1.personid = t25.personid
	where t1.FU12 = 1
	order by t1.personid;
quit;
data zzz3B;
	set zzz3A;
	if LOS12_ALL = . then LOS12_ALL = 0;
	if LOS12_OP = . and caresetting = "Outpatient" then LOS12_OP = 0;
	if LOS12_ER = . and caresetting = "Emergency" then LOS12_ER = 0;
	if LOS12_IP = . and caresetting = "Inpatient" then LOS12_IP = 0;
	if LOS12_OB = . and caresetting = "Admitted f" then LOS12_OB = 0;
run;
* R = 2929 *;

proc means data=zzz3B n nmiss mean std median q1 q3 maxdec=1;
	class LVP_ALB;
	var LOS:;
run;
proc means data=zzz3B n nmiss mean std median q1 q3 maxdec=1;
	var LOS:;
run;

*********************************************  End  *********************************************;

/*
LOS3_ALL
LOS3_OP
LOS3_ER
LOS3_IP
LOS3_OB

LOS6_ALL
LOS6_OP
LOS6_ER
LOS6_IP
LOS6_OB

LOS12_ALL
LOS12_OP
LOS12_ER
LOS12_IP
LOS12_OB
*/


/*
proc sql;
	select  n(distinct personid) as P,
			n(distinct encounterid) as E
	from temp1;
quit;

proc sql;
	select  LVP_ALB,
			n(distinct personid) as P,
			n(distinct encounterid) as E
	from temp1
	group by LVP_ALB
	order by LVP_ALB;
quit;

proc sql;
	select  caresetting,
			LVP_ALB,
			n(distinct personid) as P,
			n(distinct encounterid) as E
	from temp1
	group by caresetting, LVP_ALB
	order by caresetting, LVP_ALB;
quit;
*/
