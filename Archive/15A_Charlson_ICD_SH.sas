﻿/*---------------------------------------------------------------------------------
Project:            0979-grifols-albumin-lvp

Program:            ____ 

Function:           ____

Dependencies:		____

Dependents:			____

Author:             ____

Date Completed:     ____

SAS Version:        ____   

Platform:           ____

Macros Called:      ____ 

Macro Parameters:   ____

Comments:           ____

Example:			____

Revisions:
date	name	what
210218	SH		Initial verson
---------------------------------------------------------------------------------*/


* ==================================  Step 1. ICD-10  ================================== *;
/*
2021 ICD-10-CM was used.
https://www.cms.gov/medicare/icd-10/2021-icd-10-cm
2021 Code Descriptions in Tabular Order – Updated 12/16/2020 (ZIP)
icd10cm_codes_2021.txt

ICD-10 codes in Table 1 from the Quan's 2005 paper (**) were used to identify 17 Charlson comorbidities.
A SAS code from the MCHP website was used with minor modifications:  
http://mchp-appserv.cpe.umanitoba.ca/Upload/SAS/ICD10_Charlson.sas.txt

** Quan et al., "Coding algorithms for defining comorbidities
in ICD-9-CM and ICD-10 administrative data." Med Care, 2005 Nov;43(11):1130-9.
*/

data ten1;
	length
		Code $ 7
		Description $ 256
	;
	infile
		"C:\Users\&SYSUSERID.\Documents\0979-grifols-albumin-lvp\Input\icd10cm_codes_2021.txt"
		TRUNCOVER
		FIRSTOBS=1
	;
	input
		@1 Code $7.
		@9 Description $256.
	;
run;
* R = 72621 *;

data ten2;
	set ten1;
	DX_1 = Code;
run;

%macro ICD10_CH (library=, dataset=, output=);
data &library..&output;
	set &library..&dataset;

	/**Myocardial Infarction**/
	%LET DIS1=MI;
	%LET DC1=%STR('I21','I22','I252');
	%LET LBL1=%STR(Myocardial Infarction);

	/**Congestive Heart Failure**/
	%LET DIS2=CHF;
	%LET DC2=%STR('I43','I50','I099','I110','I130','I132',
	'I255','I420','I425','I426','I427','I428','I429','P290');
	%LET LBL2=%STR(Congestive Heart Failure);

	/**Periphral Vascular Disease**/
	%LET DIS3=PVD;
	%LET DC3=%STR('I70','I71', 'I731','I738','I739','I771',
	'I790','I792','K551','K558','K559','Z958','Z959');
	%LET LBL3=%STR(Periphral Vascular Disease);

	/**Cerebrovascular Disease**/
	%LET DIS4=CEVD;
	%LET DC4=%STR('G45','G46','I60','I61','I62','I63','I64','I65','I66','I67','I68','I69','H340');
	%LET LBL4=%STR(Cerebrovascular Disease);

	/**Dementia**/
	%LET DIS5=DEM;
	%LET DC5=%STR('F00','F01','F02','F03','G30','F051','G311');
	%LET LBL5=%STR(Dementia);

	/**Chronic Pulmonary Disease**/
	%LET DIS6=COPD;
	%LET DC6=%STR('J40','J41','J42','J43','J44','J45','J46','J47',
	'J60','J61','J62','J63','J64','J65','J66','J67',
	'I278','I279','J684','J701','J703');
	%LET LBL6=%STR(Chronic Pulmonary Disease);

	/**Connective Tissue Disease-Rheumatic Disease**/
	%LET DIS7=Rheum;
	%LET DC7=%STR('M05','M32','M33','M34','M06','M315','M351','M353','M360');
	%LET LBL7=%STR(Rheumatic Disease);

	/**Peptic Ulcer Disease**/   
	%LET DIS8=PUD;
	%LET DC8=%STR('K25','K26','K27','K28');
	%LET LBL8=%STR(Peptic Ulcer Disease);

	/**Mild Liver Disease**/
	%LET DIS9=MILDLD;
	%LET DC9=%STR('B18','K73','K74','K700','K701','K702','K703','K709',
	'K717','K713','K714','K715','K760','K762','K763','K764','K768','K769','Z944');  
	 %LET LBL9=%STR(Mild Liver Disease);

	/**Diabetes without complications**/
	%LET DIS10=DIAB_NC;
	%LET DC10=%STR('E100','E101','E106','E108','E109','E110','E111','E116','E118','E119',
	'E120','E121','E126','E128','E129',
	'E130','E131','E136','E138','E139',
	'E140','E141','E146','E148','E149');
	%LET LBL10=%STR(Diabetes without complications);

	/**Diabetes with complications**/
	%LET DIS11=DIAB_C;
	%LET DC11=%STR('E102','E103','E104','E105','E107',
	'E112','E113','E114','E115','E117',
	'E122','E123','E124','E125','E127',
	'E132','E133','E134','E135','E137',
	'E142','E143','E144','E145','E147');
	%LET LBL11=%STR(Diabetes with complications);

	/**Paraplegia and Hemiplegia**/
	%LET DIS12=PARA;
	%LET DC12=%STR('G81','G82','G041','G114','G801','G802',
	'G830','G831','G832','G833','G834','G839');    
	%LET LBL12=%STR(Paraplegia and Hemiplegia);
		
	/**Renal Disease**/
	%LET DIS13=RD;
	%LET DC13=%STR('N18','N19','N052','N053','N054','N055','N056','N057',
	'N250','I120','I131','N032','N033','N034','N035','N036','N037',
	'Z490','Z491','Z492','Z940','Z992');
	%LET LBL13=%STR(Renal Disease);

	/**Cancer**/
	%LET DIS14=CANCER;
	%LET DC14=%STR('C00','C01','C02','C03','C04','C05','C06','C07','C08','C09',
	'C10','C11','C12','C13','C14','C15','C16','C17','C18','C19',
	'C20','C21','C22','C23','C24','C25','C26',
	'C30','C31','C32','C33','C34','C37','C38','C39',
	'C40','C41','C43','C45','C46','C47','C48','C49',
	'C50','C51','C52','C53','C54','C55','C56','C57','C58',
	'C60','C61','C62','C63','C64','C65','C66','C67','C68','C69',
	'C70','C71','C72','C73','C74','C75','C76',
	'C81','C82','C83','C84','C85','C88',
	'C90','C91','C92','C93','C94','C95','C96','C97');
	%LET LBL14=%STR(Cancer);

	/**Moderate or Severe Liver Disease**/
	%LET DIS15=MSLD;
	%LET DC15=%STR('K704','K711','K721','K729','K765','K766','K767','I850','I859','I864','I982');
	%LET LBL15=%STR(Moderate or Severe Liver Disease);

	/**Metastatic Carcinoma**/
	%LET DIS16=METS;
	%LET DC16=%STR('C77','C78','C79','C80');
	%LET LBL16=%STR(Metastatic Carcinoma);

	/**AIDS/HIV**/
	%LET DIS17=HIV;
	%LET DC17=%STR('B20','B21','B22','B24');
	%LET LBL17=%STR(AIDS/HIV);

	%do DI=1 %to 17;/*ICD10 Charlson: 17 groups*/
		A&DI=0; 						
		%do DX=1 %to 1;	/* SH: We have only 1 DX */ 	
			B&DX=0;
			%do SN=3 %to 5;
				if substr(dx_&DX, 1, &SN) in (&&DC&DI) then C&SN = 1;
				else C&SN = 0;
				B&DX = B&DX + C&SN;
				drop C&SN;
			%end;
			A&DI = A&DI + B&DX;
			DROP B&DX;
		%end;
		if A&DI > 0 then CH_&&DIS&DI = 1;
		else CH_&&DIS&DI = 0;
		label CH_&&DIS&DI = &&LBL&DI;
		DROP A&DI;	
	%end;
run;
%mend ICD10_CH;

%ICD10_CH(library=work, dataset=ten2, output=ten3);
* R = 72621 *;

proc means data=ten3 min mean max maxdec=5;
run;

data ten4 (drop=DX_1);
	set ten3;
	format Code2 $8.;
	if substr(Code, 4, 1) = "" then Code2 = Code;
	else if substr(Code, 5, 1) = "" then Code2 = substr(Code, 1, 3) || '.' || substr(Code, 4, 1);
	else if substr(Code, 6, 1) = "" then Code2 = substr(Code, 1, 3) || '.' || substr(Code, 4, 2);
	else if substr(Code, 7, 1) = "" then Code2 = substr(Code, 1, 3) || '.' || substr(Code, 4, 3);
	else if substr(Code, 8, 1) = "" then Code2 = substr(Code, 1, 3) || '.' || substr(Code, 4, 4);
run;



* ==================================  Step 2. ICD-9  ================================== *;
/*
Version 32 ICD-9 was used.
https://www.cms.gov/Medicare/Coding/ICD9ProviderDiagnosticCodes/codes
Version 32 Full and Abbreviated Code Titles  – Effective October 1, 2014 (ZIP)
CMS32_DESC_LONG_DX.txt

Enhanced ICD-9-CM codes in Table 1 from the Quan's 2005 paper (**) were used to identify 17 Charlson comorbidities.
A SAS code from the MCHP website was used with minor modifications:  
http://mchp-appserv.cpe.umanitoba.ca/Upload/SAS/ICD9_E_Charlson.sas.txt

** Quan et al., "Coding algorithms for defining comorbidities
in ICD-9-CM and ICD-10 administrative data." Med Care, 2005 Nov;43(11):1130-9.
*/

data nine1;
	length
		Code $ 5
		Description $ 256
	;
	infile
		"C:\Users\&SYSUSERID.\Documents\0979-grifols-albumin-lvp\Input\CMS32_DESC_LONG_DX.txt"
		TRUNCOVER
		FIRSTOBS=1
	;
	input
		@1 Code $5.
		@7 Description $256.
	;
run;
* R = 14567 *;

data nine2;
	set nine1;
	DX_1 = Code;
run;

%macro ICD9_E_CH(library=, dataset=, output=);
data &library..&output;
	set &library..&dataset /*(keep=dx_1-dx_16 EXIT_ALIVE cdr_key)*/;

	/**Myocardial Infarction**/
	%LET DIS1=MI;
	%LET DC1=%STR('410','412');
	%LET LBL1=%STR(Myocardial Infarction);

	/**Congestive Heart Failure**/
	%LET DIS2=CHF;
	%LET DC2=%STR('39891','40201','40211','40291','40401','40403','40411','40413','40491','40493','4254','4255','4257','4258','4259','428');
	%LET LBL2=%STR(Congestive Heart Failure);

	/**Periphral Vascular Disease**/
	%LET DIS3=PVD;
	%LET DC3=%STR('0930','4373','440','441','4431','4432','4438','4439','4471','5571','5579','V434');
	%LET LBL3=%STR(Periphral Vascular Disease);

	/**Cerebrovascular Disease**/
	%LET DIS4=CEVD;
	%LET DC4=%STR('36234','430','431','432','433','434','435','436','437','438');
	%LET LBL4=%STR(Cerebrovascular Disease);

	/**Dementia**/
	%LET DIS5=DEM;
	%LET DC5=%STR('290','2941','3312');          
	%LET LBL5=%STR(Dementia);

	/**Chronic Pulmonary Disease**/
	%LET DIS6=COPD;
	%LET DC6=%STR('4168','4169','490','491','492','493','494','495','496','500','501','502','503','504','505','5064','5081','5088');
	%LET LBL6=%STR(Chronic Pulmonary Disease);

	/**Connective Tissue Disease-Rheumatic Disease**/
	%LET DIS7=Rheum;
	%LET DC7=%STR('4465','7100','7101','7102','7103','7104','7140','7141','7142','7148','725');
	%LET LBL7=%STR(Connective Tissue Disease-Rheumatic Disease);

	/**Peptic Ulcer Disease**/   
	%LET DIS8=PUD;
	%LET DC8=%STR('531','532','533','534');      
	%LET LBL8=%STR(Peptic Ulcer Disease);

	/**Mild Liver Disease**/
	%LET DIS9=MILDLD;
	%LET DC9=%STR('07022','07023','07032','07033','07044','07054','0706','0709','570','571','5733','5734','5738','5739','V427');
	%LET LBL9=%STR(Mild Liver Disease);

	/**Diabetes without complications**/
	%LET DIS10=DIAB_NC;
	%LET DC10=%STR('2500','2501','2502','2503','2508','2509');
	%LET LBL10=%STR(Diabetes without complications);

	/**Diabetes with complications**/
	%LET DIS11=DIAB_C;
	%LET DC11=%STR('2504','2505','2506','2507');  
	%LET LBL11=%STR(Diabetes with complications);

	/**Paraplegia and Hemiplegia**/
	%LET DIS12=PARA;
	%LET DC12=%STR('3341','342','343','3440','3441','3442','3443','3444','3445','3446','3449');
	%LET LBL12=%STR(Paraplegia and Hemiplegia);

	/**Renal Disease**/
	%LET DIS13=RD;
	%LET DC13=%STR('40301','40311','40391','40402','40403','40412','40413','40492','40493','582','5830','5831','5832','5834','5836','5837','585','586','5880','V420','V451','V56');
	%LET LBL13=%STR(Renal Disease);

	/**Cancer**/
	%LET DIS14=CANCER;
	%LET DC14=%STR('140','141','142','143','144','145','146','147','148','149','150','151','152','153','154','155','156','157','158','159','160','161','162','163','164','165','170','171','172','174','175','176','179','180','181','182','183','184','185','186','187','188','189','190','191','192','193','194','195','200','201','202','203','204','205','206','207','208','2386');
	%LET LBL14=%STR(Cancer);

	/**Moderate or Severe Liver Disease**/
	%LET DIS15=MSLD;
	%LET DC15=%STR('4560','4561','4562','5722','5723','5724','5728');
	%LET LBL15=%STR(Moderate or Severe Liver Disease);

	/**Metastatic Carcinoma**/
	%LET DIS16=METS;
	%LET DC16=%STR('196','197','198','199');      
	%LET LBL16=%STR(Metastatic Carcinoma);

	/**AIDS/HIV**/
	%LET DIS17=HIV;
	%LET DC17=%STR('042','043','044');
	%LET LBL17=%STR(AIDS/HIV);

	%do DI=1 %to 17;/*ICD9-E Charlson: 17 groups*/
		A&DI=0; 						
		%do DX=1 %to 1;	/* SH: We have only 1 DX */
			B&DX=0;
			%do SN=3 %to 5;
				if substr(dx_&DX, 1, &SN) in (&&DC&DI) then C&SN = 1;
				else C&SN = 0;
				B&DX = B&DX + C&SN;
				drop C&SN;
			%end;
			A&DI = A&DI + B&DX;
			DROP B&DX;
		%end;
		if A&DI > 0 then CH_&&DIS&DI = 1;
		else CH_&&DIS&DI = 0;
		label CH_&&DIS&DI = &&LBL&DI;
		DROP A&DI;	
	%end;

run;
%mend ICD9_E_CH;

%ICD9_E_CH(library=work, dataset=nine2, output=nine3);
* R = 14567 *;

proc means data=nine3 min mean max maxdec=5;
run;

data nine4 (drop=DX_1);
	set nine3;
	format Code2 $8.;
	if substr(Code, 4, 1) = "" then Code2 = Code;
	else if substr(Code, 5, 1) = "" then Code2 = substr(Code, 1, 3) || '.' || substr(Code, 4, 1);
	else if substr(Code, 6, 1) = "" then Code2 = substr(Code, 1, 3) || '.' || substr(Code, 4, 2);
run;



* ==================================  Step 3. Combine and Export  ================================== *;
proc sql;
	create table Charlson_ICD as
	select 	10 as Version format=2.,
			Code as Code_NoDot format=$7.,
			Code2 as Code_Dot format=$8.,
			Description,
			CH_MI,
			CH_CHF,
			CH_PVD,
			CH_CEVD,
			CH_DEM,
			CH_COPD,
			CH_Rheum,
			CH_PUD,
			CH_MILDLD,
			CH_DIAB_NC,
			CH_DIAB_C,
			CH_PARA,
			CH_RD,
			CH_CANCER,
			CH_MSLD,
			CH_METS,
			CH_HIV
	from ten4

	outer union corr
	select 	9 as Version format=2.,
			cats(Code, '09'x) as Code_NoDot format=$7.,	/* Due to leading zero issues */
			cats(Code2, '09'x) as Code_Dot format=$8.,	/* Due to leading zero issues */
			Description,
			CH_MI,
			CH_CHF,
			CH_PVD,
			CH_CEVD,
			CH_DEM,
			CH_COPD,
			CH_Rheum,
			CH_PUD,
			CH_MILDLD,
			CH_DIAB_NC,
			CH_DIAB_C,
			CH_PARA,
			CH_RD,
			CH_CANCER,
			CH_MSLD,
			CH_METS,
			CH_HIV
	from nine4

	order by 1 desc, 2, 3;
quit;
* R = 87188 *;

data check;
	set Charlson_ICD;
	if 	sum(CH_MI,
		CH_CHF,
		CH_PVD,
		CH_CEVD,
		CH_DEM,
		CH_COPD,
		CH_Rheum,
		CH_PUD,
		CH_MILDLD,
		CH_DIAB_NC,
		CH_DIAB_C,
		CH_PARA,
		CH_RD,
		CH_CANCER,
		CH_MSLD,
		CH_METS,
		CH_HIV) > 1;
run;
* R = 4 *;
* Some ICD codes are associated with two different Charlson conditions. *;

proc export data=Charlson_ICD
	dbms=csv
	outfile="F:\0979\Output\Charlson_ICD.csv"
	replace;
run;

data Charlson_ICD9;
	set Charlson_ICD;
	if Version = 9;
run;
proc export data=Charlson_ICD9
	dbms=csv
	outfile="F:\0979\Output\Charlson_ICD9.csv"
	replace;
run;

data Charlson_ICD10;
	set Charlson_ICD;
	if Version = 10;
run;
proc export data=Charlson_ICD10
	dbms=csv
	outfile="F:\0979\Output\Charlson_ICD10.csv"
	replace;
run;

*********************************************  End  *********************************************;
